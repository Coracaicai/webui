import React from 'react';

import './App.css';
import { Router } from "@reach/router";


// pages
import HomePage from './components/pages/HomePage';

import SubmitPage from './components/pages/SubmitPage';

import ReportingPage from './components/pages/ReportingPage';
import JobReportPage from './components/pages/JobReportPage';
import SettingsPage from './components/pages/SettingsPage';

// page framework
import Header from './components/sections/Header';
import MainNav from './components/sections/MainNav';
import BreadCrumbs from './components/sections/BreadCrumbs';
import Footer from './components/sections/Footer';



function App() {


  return (
    <>
        <div className="App">

            <div className="Header">
                <Header />
            </div>

            <div className="MainNav">
                <MainNav />
            </div>

            <div className="BreadCrumbs">
                <BreadCrumbs />
            </div>

            <div className="PageContent">

                <Router>

                    <HomePage path="/" />

                    <SubmitPage path="submit" />

                    <ReportingPage path="/reporting" />

                    <JobReportPage path="/reporting/:JobId" />

                    <SettingsPage path="/settings" />

                </Router>
            </div>

            <div className="Footer">
                <Footer />
            </div>
        </div>
    </>
  );
}

export default App;
