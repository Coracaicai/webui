 /**
 * The TextfileSubmitComponant script builds the drag and drop section of the submit page. This
 * allows a user to easily submit files to the server. It contains both Student work as well as
 * a javascript plug from https://www.digitalocean.com/community/tutorials/react-react-dropzone
 *
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 *
 * @edits :: 2021-10-2 :: Ian Blott ::  Changed submit form from a browse & submit HTML form
 *                                      to include the drag and drop plugin
 * @edits :: 2021-10-11 :: Ian Blott :: add dynamic URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 */


// initialise state and onload functionality
import React, {useCallback, useEffect, useMemo, useState} from 'react';

// import dropzone scripts
import {useDropzone} from 'react-dropzone';

// import axios scripts for API calls
import axios from "axios";

// import icons from react-icons
import {AiOutlineLoading,AiFillFileText} from "react-icons/ai";

// import settings JSON file
import SettingsJSON from '../../components/Settings/SiteSettings.json';

// drop zone style - NOT STUDENT WORD - See comment at top of script
const baseStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',

    transition: 'border .3s ease-in-out'
};

const activeStyle = {
    borderColor: '#2196f3'
};

const acceptStyle = {
    borderColor: '#00e676'
};

const rejectStyle = {
    borderColor: '#ff1744'
};
// drop zone style - NOT STUDENT WORD - See comment at top of script


function TextfileSubmitComponant(props) {

    // array of files being submitted
    const [files, setFiles] = useState([]);

    // form style
    const [PageMessageType, setPageMessageType] = useState("");

    // mesage string
    const [PageMessage, setPageMessage] = useState("");

    // button string
    const [SubmitButtonMessage, setSubmitButtonMessage] = useState("");

    // button style
    const [ButtonClass, setButtonClass] = useState("");

    // laoding icon boolean
    const [ShowLoadingIcon, setShowLoadingIcon] = useState(false);

    // create response for on drop - modified version of 3rd party work
    const onDrop = useCallback(acceptedFiles => {
        setPageMessageType("");
        setPageMessage("");
        setButtonClass("success");
        setSubmitButtonMessage("Submit");
        setFiles(acceptedFiles.map(file => Object.assign(file, {
            preview: URL.createObjectURL(file)
        })));
    }, []);

// drop zone style & functions - NOT STUDENT WORD - See comment at top of script
    const {
        getRootProps,
        getInputProps,
        isDragActive,
        isDragAccept,
        isDragReject
    } = useDropzone({
        onDrop,
        accept: '.txt'
    });

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isDragActive,
        isDragReject,
        isDragAccept
    ]);

    // the thumb area is Drop Zone but modified by students.
    let thumbs = files.map(file => (
        <div key={file.name}>
            <AiFillFileText /> {file.name}
        </div>
    ));

    // clean up
    useEffect(() => () => {
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);
// drop zone style - NOT STUDENT WORD - See comment at top of script


    // create function to perform the submit button actions
    const submitFile = (e) => {

        // stop the browser from doing its default action on button submit
        e.preventDefault();

        // set a prompt in state
        setSubmitButtonMessage("Submit...");

        // set the button style in state
        setButtonClass("info");

        // set the state to show the loading icon
        setShowLoadingIcon(true);

        // get the deployment details for the API URL
        const REACT_APP_API_GATEWAY_ID  = process.env.REACT_APP_API_GATEWAY_ID

        // construct the API endpoint URL
        const AWS_Post_URL = "https://"+REACT_APP_API_GATEWAY_ID+".execute-api.ap-southeast-2.amazonaws.com/Stage" + SettingsJSON['TextFileSubmit']['value'];

        // post the data to the API!
        axios({
            method: "POST",
            url: AWS_Post_URL,
            data: files[0],
            headers: {'content-type': 'multipart/form-data'}
        })
            // process the servers response
            .then((resultData) => {

                // define the response into a variable
                let result = resultData['data'];

                // if worked
                if (result['status'] === "Success") {
                    // change the message type state
                    setPageMessageType("success");

                    // set the message in state
                    setPageMessage("File Submitted! - File name: " + result['id'] + '.txt');

                    // show / hide loading icon
                    setShowLoadingIcon(false);

                    //  reset submit message
                    setSubmitButtonMessage("");
                } else {
                    // change the message type state
                    setPageMessageType("danger");

                    // set the message in state
                    setPageMessage("E001 Bad server response: " + result['message']);

                    // show / hide loading icon
                    setShowLoadingIcon(false);

                    // clear the browsers file list
                    setFiles([]);

                    //  reset submit message
                    setSubmitButtonMessage("");
                }
            })
            .catch((error) => {
                // change the message type state
                setPageMessageType("warning");

                // set the message in state
                setPageMessage("E002 : " + error);

                // show / hide loading icon
                setShowLoadingIcon(false);

                // clear the browsers file list
                setFiles([]);

                //  reset submit message
                setSubmitButtonMessage("");
            });
    }

    // show the page!!
    return (

        <fieldset>
            <div className="form-group">

                <label htmlFor="formFile" className="form-label mt-4">Upload a Text File for processing</label>

                <section>
                    <div className="ocr-drag-drop" {...getRootProps({style})}>
                        <input {...getInputProps()} />
                        <div>Drag and drop your text file here.</div>

                    </div>

                    <aside className="ocr-upload-preview mt-3 mb-3 ">
                        {PageMessageType !== "" &&
                        <div className={"alert alert-dismissible alert-" + PageMessageType}>
                            <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                            <h4 className="alert-heading">{PageMessageType}!</h4>
                            <p className="mb-0">
                                {PageMessage}
                                {
                                    (PageMessageType === "danger" || PageMessageType === "warning") &&
                                    <span><br/>Please Try again.</span>
                                }
                            </p>
                        </div>

                        }
                        {thumbs}
                    </aside>

                    {
                        SubmitButtonMessage !== "" &&
                        <div className="ocr-form-submit">
                            <button type="submit" className={"btn btn-" + ButtonClass}
                                    onClick={e => submitFile(e)}>{SubmitButtonMessage}</button>
                            {
                                ShowLoadingIcon &&
                                <AiOutlineLoading className="rotate ml-2"/>
                            }
                        </div>
                    }
                </section>
            </div>

        </fieldset>


    )
}

export default TextfileSubmitComponant;
