/**
 * The Header script builds a small section at the top of each page that contains the spatial logo
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */


import React from 'react';


function Header() {
    return (
            <header className="mx-auto text-center mt-4 mb-4">
                <img src="https://www.spatial.nsw.gov.au/__data/assets/image/0008/223973/logo.png" alt="Spatial Logo" />
            </header>
    );
}

export default Header;