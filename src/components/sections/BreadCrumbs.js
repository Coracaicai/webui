/**
 * The BreadCrumbs script builds a small navigation tool that reflects the location of
 * the current page and allows a user to 'step up' the tree structure of the site
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-9-15 :: Ian B :: Moved from if statements to a simple Reach/Route
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

import React from 'react';

// import react/reouter so we can determine what page we are on
import { Link, Router } from "@reach/router";

function BreadCrumbs (  ){
    return(
            <div className="container mt-3">

                <ol className="breadcrumb">

                    <HomePage />&nbsp;&nbsp;/&nbsp;&nbsp;

                    <Router>
                        <HomePage path="/" />
                        <SubmitPage path="/submit" />
                        <ReportingPage path="/reporting" />
                        <ReportPage path="/reporting/:JobId" />
                        <SettingsPage path="/settings" />
                    </Router>

                </ol>
            </div>
    );

}

// build all the links!!

const HomePage = () => (
    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
);

const SubmitPage = () => (
    <li className="breadcrumb-item"><Link to="/submit/">Submit</Link></li>
);

const ReportingPage = () => (
    <li className="breadcrumb-item"><Link to="/reporting/">Reporting</Link></li>
);

const ReportPage = (e) => (
    <li className="breadcrumb-item"><Link to="/reporting">Reporting</Link>&nbsp;&nbsp;/&nbsp;&nbsp;<Link to={"/reporting/"+e.JobId}>{e.JobId}</Link></li>
);

const SettingsPage = () => (
    <li className="breadcrumb-item"><Link to="/settings">Settings</Link></li>
);

export default BreadCrumbs;
