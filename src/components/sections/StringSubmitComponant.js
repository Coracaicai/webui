/**
 * The StringSubmitComponant loops over the report data set and produces the table that
 * represents each job in the system
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: add dynamic URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

// initialise state
import React, {useState} from 'react';

// import axios scripts for API calls
import axios from "axios";

// import icons from react-icons
import {AiOutlineLoading} from "react-icons/ai";

// import settings JSON file
import SettingsJSON from '../../components/Settings/SiteSettings.json';

function StringSubmitComponant() {

    // raw string to submit to API
    const [RawString, setRawString] = useState("");

    // form style
    const [PageMessageType, setPageMessageType] = useState("");

    // mesage string
    const [PageMessage, setPageMessage] = useState("");

    // button string
    const [SubmitButtonMessage, setSubmitButtonMessage] = useState("");

    // button style
    const [ButtonClass, setButtonClass] = useState("");

    // loading icon boolean
    const [ShowLoadingIcon, setShowLoadingIcon] = useState(false);

    const submitString = (e) => {

        // stop the browser from doing its default action on button submit
        e.preventDefault();

        // set a prompt in state
        setSubmitButtonMessage("Submit...");

        // set the button style in state
        setButtonClass("info");

        // set the state to show the loading icon
        setShowLoadingIcon(true);

        // get the deployment details for the API URL
        const REACT_APP_API_GATEWAY_ID  = process.env.REACT_APP_API_GATEWAY_ID

        // construct the API endpoint URL
        const AWS_Post_URL = "https://"+REACT_APP_API_GATEWAY_ID+".execute-api.ap-southeast-2.amazonaws.com/Stage" + SettingsJSON['StringSubmit']['value'];

        // Create an object of formData
        const fmData = new FormData();
        fmData.append("string", RawString);

        // post the data to the API!
        axios({
            method: "POST",
            url: AWS_Post_URL,
            data: RawString,
            headers: {'content-type': 'multipart/form-data'}
        })
        // process the servers response
        .then((resultData) => {

            // define the response into a variable
            let result = resultData['data'];

            if (result['status'] === "Success") {

                // change the message type state
                setPageMessageType("success");

                // set the message in state
                setPageMessage("String Submitted, Job number: " + result['id']);

                // show / hide loading icon
                setShowLoadingIcon(false);

                //  reset submit message
                setSubmitButtonMessage("Submit");

                // clear form field
                setRawString("");
            } else {

                // change the message type state
                setPageMessageType("danger");

                // set the message in state
                setPageMessage("E001 Bad server response: " + result['message']);

                // show / hide loading icon
                setShowLoadingIcon(false);

                //  reset submit message
                setSubmitButtonMessage("Submit");

                // clear form field
                setRawString("");
            }
        })
        .catch((error) => {

            // change the message type state
            setPageMessageType("warning");

            // set the message in state
            setPageMessage("E002 : " + error);

            // show / hide loading icon
            setShowLoadingIcon(false);

            //  reset submit message
            setSubmitButtonMessage("Submit");

            // clear form field
            setRawString("");
        });

    }

    // react on string change - changes f orm button
    const stringChange = (msg) => {
        // set message
        setRawString(msg);

        // place text in the button
        setSubmitButtonMessage("Submit");

        // sets the colour
        setButtonClass("info");
    }

    // create the page!!
    return (
        <fieldset>
            <div className="form-group">
                <label htmlFor="inputDescription" className="form-label mt-4">Description</label>
                <textarea
                    className="form-control"
                    id="inputDescription"
                    rows="6"
                    data-tid="avy"
                    onChange={(e) => stringChange(e.target.value)}
                >
                </textarea>
                <small id="emailHelp" className="form-text text-muted">Type or Paste your text</small>
            </div>


            <aside className="ocr-upload-preview mt-3 mb-3 ">
                {PageMessageType !== "" &&
                <div className={"alert alert-dismissible alert-" + PageMessageType}>
                    <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    <h4 className="alert-heading">{PageMessageType}!</h4>
                    <p className="mb-0">
                        {PageMessage}
                        {
                            (PageMessageType === "danger" || PageMessageType === "warning") &&
                            <span><br/>Please Try again.</span>
                        }
                    </p>
                </div>

                }
            </aside>
            {
                SubmitButtonMessage !== "" &&
                <div className="ocr-form-submit">
                    <button type="submit" className={"btn btn-" + ButtonClass}
                            onClick={e => submitString(e)}>{SubmitButtonMessage}</button>
                    {
                        ShowLoadingIcon &&
                        <AiOutlineLoading className="rotate ml-2"/>
                    }
                </div>
            }
        </fieldset>

    );

}

export default StringSubmitComponant;