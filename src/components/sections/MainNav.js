/**
 * The MainNav script builds core navigation section of the page and controls the main section being displayed
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */


// initialise state
import React, {useState} from 'react';

// initialise reach/router
import {Link} from "@reach/router";

function MainNav() {

    // store active tab in a state
    const [ActivePage, setActivePage] = useState("home");

    // change the page
    const switchPage = (page) =>{
        setActivePage(page);
    }

    return (
            <nav className="navbar navbar-expand-lg navbar-light ">
                <div className="container-fluid">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarColor03"
                            aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarColor03">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <Link onClick={ () => switchPage("home") }
                                      className={ActivePage === "home" ? "nav-link  active" : " nav-link "}
                                      to="/">Home</Link>
                            </li>


                            <li className="nav-item mx-3">
                                <Link onClick={ () => switchPage("submit") }
                                      className={ActivePage === "submit" ? "nav-link  active" : " nav-link "}
                                      to="/submit">Submit</Link>
                            </li>

                            <li className="nav-item mx-3">
                                <Link onClick={ () => switchPage("reporting") }
                                      className={ActivePage === "reporting" ? "nav-link  active" : " nav-link "}
                                      to="/reporting">Reporting</Link>
                            </li>

                            <li className="nav-item mx-3">
                                <Link onClick={ () => switchPage("settings") }
                                      className={ActivePage === "settings" ? "nav-link  active" : " nav-link "}
                                      to="/settings">Settings</Link>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>
        );
    }

export default MainNav;