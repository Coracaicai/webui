/**
 * The ReportListTable loops over the report data set and produces the table that
 * represents each job in the system
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */


import React from 'react';

// import the REport list file
import ReportListItem from "../chunks/ReportListItem.js";

function ReportListTable(props) {

    // store prop data
    const reportIDs = props.ReportIDs;

    // store prop data
    const reportList = props.ReportList;

    // store prop data
    const lenny = reportIDs.length;

    // build the page!
    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">DateTime</th>
                </tr>

            </thead>
            <tbody>

                {reportIDs.map((id) => <ReportListItem
                        key={id}
                        ReportID={id}
                        ReportData={reportList[id]}
                    />
                )}

                {
                    lenny === 0 &&
                    <tr>
                        <td colSpan="3">
                            <div className="no-items-to-show">
                                <p>No results to show</p>
                            </div>
                        </td>
                    </tr>
                }

            </tbody>
        </table>

    )
}


export default ReportListTable;