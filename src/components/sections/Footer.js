/**
 * The Footer script builds a small section at the base of each page that contains the copy right information
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

import React from 'react';

function Footer() {
    return (
        <div className="container m-auto">
            <div className="text-center mt-2 mb-2">
                <a href="https://www.nsw.gov.au/" rel="noreferrer" target="_blank">NSW Government</a>
                <span> Copyright &copy; Spatial Services 2021</span>
            </div>
        </div>
    );
}

export default Footer;