/**
 * The JobReportPage script builds the surrounding content of an individual job report.
 * It will collect the data from the API and pass it to a number of JobReportDetails sections
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: add dynamic URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *           2021-10-24 :: Ian Blott :: Fixed minor bug where duplicate keys being created for JobReportDetails
 *
 */

// initial state
import React, {useState,useEffect} from 'react';

// get the setting JSON data
import SettingsJSON from "../Settings/SiteSettings.json";

// import the JobReportDetails file
import JobReportDetails from "../chunks/JobReportDetails.js";

function JobReportPage(props) {

    // store the Job ID in a state variable
    const [JobData,SetJobData]=useState({});

    // get the env varialbe for the API end point
    const REACT_APP_API_GATEWAY_ID  = process.env.REACT_APP_API_GATEWAY_ID;

    // build the API end point URL
    const url = "https://"+REACT_APP_API_GATEWAY_ID+".execute-api.ap-southeast-2.amazonaws.com/Stage" +
                SettingsJSON['JobReport']['value']+
                '?searchable='+props.JobId;

    // on load, fetch the job data from the API
    useEffect(()=>{
        fetch(url,{mode:'cors'})
        .then(function(response){
            return response.json();
        })
        .then(function(myJson) {
            SetJobData(myJson);
        });
    },[url])

    // pre define an array ready to show on the page
    const final = [];

    // construct the JobReportDetails sections
    for (let x = 0; x < JobData.length; x++) {
        final.push(<JobReportDetails
            key={JobData[x].Lambda+"_"+JobData[x].Id}
            jobId={JobData[x].Id}
            Lambda={JobData[x].Lambda}
            InputFile = {JobData[x].InputFile}
            OutputFile = {JobData[x].OutputFile}
            StartDateTime = {JobData[x].StartDateTime}
        />);
    }

    return (
        <div className="container">
            <div className="card border-secondary mb-5 ">
                <h4 className="card-header">Job Report</h4>
                <div className="card-body">
                    <h5>Report for job: {props.JobId}</h5>
                    {final}
                </div>
            </div>
        </div>
    )
}

export default JobReportPage;