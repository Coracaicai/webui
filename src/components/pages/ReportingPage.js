/**
 * The ReportingPage script builds the core reporting page by producing the Accordion and
 * calling child reporting segments. Each section will have an array sent to it that has
 * been filtered / assigned to it on this page
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: add dynamic URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

// allow for state
import React, {useState, useEffect} from 'react';

// get the setting JSON data
import SettingsJSON from "../Settings/SiteSettings.json";

// import the JobReportDetails file
import ReportListTable from "../sections/ReportListTable.js";

function ReportingPage() {

    // a state to store what part of the Accoridan is to be show
    const [ActiveTab, setActiveTab] = useState("SuccessReports");

    // an array of reports for all the reports
    const [Reports, setReports] = useState([]);

    // an array of reports for the successful reports section
    const [StringReports, setStringReportsReports] = useState([]);

    // an array of reports for the textract section
    const [WebUIImageReports, setWebUIImageReports] = useState([]);

    // an array of reports for the Human Intervention section
    const [HumanInterventionReports, setHumanInterventionReports] = useState([]);

    // collect the data from the API
    const getData = () => {

        // get the env variable for the API end point
        const REACT_APP_API_GATEWAY_ID  = process.env.REACT_APP_API_GATEWAY_ID;

        // build the API end point URL
        const url = "https://"+REACT_APP_API_GATEWAY_ID+".execute-api.ap-southeast-2.amazonaws.com/Stage" + SettingsJSON['JobList']['value'];


        fetch(url, {mode: 'cors'})
            .then(function (response) {
                // decode the response into as JSON object
                return response.json();
            })
            .then(function (myJson) {
                // initiate variables
                let tempReport = [];
                let tempHumanInterventionReport = [];
                let tempWebUIImageReports = [];
                let tempStringReports = [];

                // loop over the reports array
                myJson.forEach((item) => {
                    // initiate / reset variable in the array
                    let arr = []

                    // assign the ID
                    arr.Id = item.Id;

                    // assign the Lambda
                    arr.Lambda = item.Lambda;

                    // assign the Date Time
                    arr.DateTime = (item.StartDateTime !== undefined ? item.StartDateTime : "01/01/2020 00:00:00");


                    // store the report data in its respective state Array
                    if( item.Lambda === 'HumanIntervention'){
                        tempHumanInterventionReport.push(item.Id);
                    } else if( item.Lambda === 'WebUIImage'){
                        tempWebUIImageReports.push(item.Id);
                    } else {
                        tempStringReports.push(item.Id);
                    }

                    // makes sure we never have an undefined array
                    if(tempReport[item.Id] === undefined ) {
                        tempReport[item.Id] = []
                    }

                    // assign the the objec into the array
                    tempReport[item.Id][item.Lambda] = item;
                });

                // make sure they are unique, we dont need repeats
                var filteredtempWebUIImageReports = tempWebUIImageReports.filter(function(item, pos){
                    return tempWebUIImageReports.indexOf(item)=== pos;
                });
                setWebUIImageReports(filteredtempWebUIImageReports);

                // make sure they are unique, we dont need repeats
                var filteredtempHumanInterventionReport = tempHumanInterventionReport.filter(function(item, pos){
                    return tempHumanInterventionReport.indexOf(item)=== pos;
                });
                setHumanInterventionReports(filteredtempHumanInterventionReport);

                // make sure they are unique, we dont need repeats
                var filteredtempStringReports = tempStringReports.filter(function(item, pos){
                    return tempStringReports.indexOf(item)=== pos;
                });
                setStringReportsReports(filteredtempStringReports);

                // set the final array structure back into the state variable
                setReports(tempReport);

            });
    }

    // on page load, get the data from the API
    useEffect(() => {
        getData()
    }, [])


    // build the page!!
    return (
        <div className="container">
            <div className="accordion-item">
                <h2 className="accordion-header" id="SuccessReportsHeading">
                    <button className={`accordion-button ${ActiveTab === "SuccessReports" ? "" : "collapsed"}`}
                            type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextString"
                            aria-expanded={ActiveTab === "SuccessReports" ? "true" : "false"}
                            aria-controls="collapseSuccessReports"
                            onClick={() => setActiveTab("SuccessReports")}
                    >
                        Successful Reports ({StringReports.length})
                    </button>
                </h2>
                <div id="collapseLinks"
                     className={`accordion-collapse collapse  ${ActiveTab === "SuccessReports" ? "show" : ""}`}
                     aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <ReportListTable
                            ReportIDs={StringReports}
                            ReportList={Reports}
                        />
                    </div>
                </div>
            </div>


            <div className="accordion-item">
                <h2 className="accordion-header" id="TextractReportsHeading">
                    <button className={`accordion-button ${ActiveTab === "TextractReports" ? "" : "collapsed"}`}
                            type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextString"
                            aria-expanded={ActiveTab === "TextractReports" ? "true" : "false"}
                            aria-controls="collapseTextractReports"
                            onClick={() => setActiveTab("TextractReports")}
                    >
                        Textract Reports ({WebUIImageReports.length})
                    </button>
                </h2>
                <div id="collapseLinks"
                     className={`accordion-collapse collapse  ${ActiveTab === "TextractReports" ? "show" : ""}`}
                     aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <ReportListTable
                            ReportIDs={WebUIImageReports}
                            ReportList={Reports}
                        />
                    </div>
                </div>
            </div>

            <div className="accordion-item mb-5">
                <h2 className="accordion-header" id="HumanInterventionReportsHeading">
                    <button
                        className={`accordion-button ${ActiveTab === "HumanInterventionReports" ? "" : "collapsed"}`}
                        type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextString"
                        aria-expanded={ActiveTab === "HumanInterventionReports" ? "true" : "false"}
                        aria-controls="collapseHumanInterventionReports"
                        onClick={() => setActiveTab("HumanInterventionReports")}
                    >
                        Human Intervention Reports ({HumanInterventionReports.length})
                    </button>
                </h2>
                <div id="collapseLinks"
                     className={`accordion-collapse collapse  ${ActiveTab === "HumanInterventionReports" ? "show" : ""}`}
                     aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <ReportListTable
                            ReportIDs={HumanInterventionReports}
                            ReportList={Reports}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ReportingPage;
