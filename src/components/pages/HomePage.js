/**
 * The HomePage script builds the intial page and shows the intro Accordion and its child contents. this is the default
 * view of the web UI
 * There is little code other than raw HTML in this page - only the decision as to what Accordion section to show
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

import React, {useState} from 'react';
import {Link} from "@reach/router";

function HomePage() {

    // store in state the current active Accordion tab
    const [ActiveTab, setActiveTab] = useState("intro");

    return (
        <div className="container">
            <div className="accordion" id="accordionExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="introHeading">
                        <button className={`accordion-button ${ActiveTab === "intro" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapseIntro"
                                aria-expanded={ActiveTab === "intro" ? "true" : "false"}
                                aria-controls="collapseIntro"
                                onClick={() => setActiveTab("intro")}
                        >
                            Intro
                        </button>
                    </h2>
                    <div id="collapseIntro"
                         className={`accordion-collapse collapse ${ActiveTab === "intro" ? "show" : ""}`}
                         aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body text-center">
                            <img src="/Toremiian_3.png" alt="team logo"/>
                            <p className="mt-1">Ian Blott - Regan Frank - Tomas Godfrey - Michael Senkic</p>
                        </div>
                    </div>
                </div>


                <div className="accordion-item">
                    <h2 className="accordion-header" id="aboutHeading">
                        <button className={`accordion-button ${ActiveTab === "about" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapseAbout"
                                aria-expanded={ActiveTab === "about" ? "true" : "false"}
                                aria-controls="collapseAbout"
                                onClick={() => setActiveTab("about")}
                        >
                            About
                        </button>
                    </h2>
                    <div id="collapseAbout"
                         className={`accordion-collapse collapse  ${ActiveTab === "about" ? "show" : ""}`}
                         aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <p>Working with the NSW government department, Spatial Services | Department of Customer
                                Service
                                (DCS - Spatial), the team will create a machine learning algorithm to decompose, analyse
                                and
                                categorise already digitised data from survey maps and learn how surveyors wrote their
                                survey descriptions over time.</p>
                        </div>
                    </div>
                </div>



                <div className="accordion-item">
                    <h2 className="accordion-header" id="linksHeading">
                        <button className={`accordion-button ${ActiveTab === "links" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapseLinks"
                                aria-expanded={ActiveTab === "links" ? "true" : "false"}
                                aria-controls="collapseLinks"
                                onClick={() => setActiveTab("links")}
                        >
                            Links
                        </button>
                    </h2>
                    <div id="collapseLinks"
                         className={`accordion-collapse collapse ${ActiveTab === "links" ? "show" : ""}`}
                         aria-labelledby="headingLinks" data-bs-parent="#accordionExample">
                        <div className="accordion-body">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Link</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Code Repository</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://bitbucket.org/toremiian">BitBucket</Link></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Document Repository</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://toremiian.atlassian.net/wiki/spaces/G3/">Confluence</Link>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Push Communication</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://discord.gg/YsVFPzU8">Discord</Link></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Team charter</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://toremiian.atlassian.net/wiki/spaces/G3/pages/393268/Team+Charter">Team
                                            Charter</Link></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Project Proposal</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://toremiian.atlassian.net/wiki/spaces/G3/pages/17956887/">Project
                                            Proposal</Link></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">LCAM</th>
                                        <td><Link rel="noreferrer" target="_blank"
                                                  to="https://toremiian.atlassian.net/wiki/spaces/G3/pages/405602305/LCAM+Resubmission">LCAM
                                            Resubmission</Link></td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>



            <div className="accordion-item">
                <h2 className="accordion-header" id="stakeholdersHeading">
                    <button className={`accordion-button ${ActiveTab === "stakeholders" ? "" : "collapsed"}`}
                            type="button" data-bs-toggle="collapse" data-bs-target="#collapsestakeholders"
                            aria-expanded={ActiveTab === "stakeholders" ? "true" : "false"}
                            aria-controls="collapsestakeholders"
                            onClick={() => setActiveTab("stakeholders")}>
                        Stake Holders
                    </button>
                </h2>
                <div id="collapsestakeholders"
                     className={`accordion-collapse collapse   ${ActiveTab === "stakeholders" ? "show" : ""}`}
                     aria-labelledby="collapsestakeholders" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Responsibilities</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Lars Hansen</th>
                                    <td>Director of Information Services, Spatial Services</td>
                                    <td>Lars and his team are responsible for the management and processing of digital data
                                        and the information technology platforms that support the Spatial Services business.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Annette McArdle</th>
                                    <td>TBA</td>
                                    <td>TBA</td>
                                </tr>
                                <tr>
                                    <th scope="row">Simon Reynolds</th>
                                    <td>Team Leader Development at Spatial Services</td>
                                    <td>Monitoring project progress and communicating with the students, as a first line of
                                        contact for the project.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Maria Jansen</th>
                                    <td>Project Officer</td>
                                    <td>Coordination of communication and meetings between student development team and DCS
                                        Spatial Services employees.
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">David Tien</th>
                                    <td>CSU Lecturer</td>
                                    <td>Project Supervisor</td>
                                </tr>
                                <tr>
                                    <th scope="row">Michael Senkic</th>
                                    <td>CSU Development Team</td>
                                    <td> Project Design and Development</td>
                                </tr>
                                <tr>
                                    <th scope="row">Thomas Godfrey</th>
                                    <td>CSU Development Team</td>
                                    <td> Project Design and Development</td>
                                </tr>
                                <tr>
                                    <th scope="row">Ian Blott</th>
                                    <td>CSU Development Team</td>
                                    <td>Project Design and Development</td>
                                </tr>
                                <tr>
                                    <th scope="row">Regan Frank</th>
                                    <td>CSU Development Team</td>
                                    <td>Project Design and Development</td>
                                </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomePage;
