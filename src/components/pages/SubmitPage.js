/**
 * The SubmitPage script builds the core submission page by producing the accordion and
 * calling child submit segments
 *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: removed a number of end points as well as the hard coded
 *                                      URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */


// allow for state
import React, {useState} from 'react';

// import the text file submit form
import TextfileSubmitComponant from '../sections/TextfileSubmitComponant';

// import the string submit form
import StringSubmitComponant from '../sections/StringSubmitComponant';

// import the image submit form
import ImageSubmitComponent from '../sections/ImageSubmitComponent';


function SubmitPage() {

    // store the Accordion active tab
    const [ActiveTab, setActiveTab] = useState("image");

    // build the page!
    return (
        <div className="container">
            <div className="tileContent">
                <div className="tileTable">

                    <div className="tileRow">
                        <div className={`tileCell ${ActiveTab === "image" ? "tab-selected" : ""}`} >
                            <div className="tileInner" id="tc11">
                                <button className="top-links"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#collapseimage"
                                        aria-expanded={ActiveTab === "image" ? "true" : "false"}
                                        aria-controls="collapseIntro"
                                        onClick={() => setActiveTab("image")}
                                >
                                    <div className="tileImage">
                                        <img
                                            src="https://www.spatial.nsw.gov.au/__data/assets/image/0003/225264/SG-Directions.jpg" alt="" title="Customer Hub"/>
                                    </div>
                                    <p className="tileTitle">Submit an Image to Textract</p>
                                    <div className="tileText">
                                        <p>Submit a Spatial Survey plan to extract its text data</p>

                                    </div>
                                </button>
                            </div>
                        </div>


                        <div className={`tileCell ${ActiveTab === "textString" ? "tab-selected" : ""}`} >
                            <div className="tileInner" id="tc12">
                                <button className="top-links"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextString"
                                        aria-expanded={ActiveTab === "textString" ? "true" : "false"}
                                        aria-controls="collapseAbout"
                                        onClick={() => setActiveTab("textString")}
                                >
                                    <div className="tileImage"><img
                                        src="https://www.spatial.nsw.gov.au/__data/assets/image/0013/224041/News.jpg"alt="" title="Portal"/>
                                    </div>
                                    <p className="tileTitle">Post Raw Text To Comprehend</p>
                                    <div className="tileText">
                                        <p>Submit a typed line or lines of description text to be processed</p>
                                    </div>
                                </button>
                            </div>
                        </div>

                        <div className={`tileCell ${ActiveTab === "textFile" ? "tab-selected" : ""}`} >
                            <div className="tileInner" id="tc13">

                                <button className="top-links"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextFile"
                                        aria-expanded={ActiveTab === "textFile" ? "true" : "false"}
                                        aria-controls="collapseLinks"
                                        onClick={() => setActiveTab("textFile")}
                                >

                                    <div className="tileImage">
                                        <img
                                            src="https://www.spatial.nsw.gov.au/__data/assets/image/0016/224044/ServiceStatus.jpg" alt="News" title="News"/>
                                    </div>
                                    <p className="tileTitle">Post Text File To Comprehend</p>
                                    <div className="tileText">
                                        <p>Submit a bulk file with description text to be processed</p>
                                    </div>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>


            <div className="accordion" id="accordionExample">
                <div className="accordion-item">
                    <h2 className="accordion-header" id="introHeading">
                        <button className={`accordion-button ${ActiveTab === "image" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapseimage"
                                aria-expanded={ActiveTab === "image" ? "true" : "false"}
                                aria-controls="collapseIntro"
                                onClick={() => setActiveTab("image")}
                        >
                            Submit an Image to Textract
                        </button>
                    </h2>
                    <div id="collapseIntro"
                         className={`accordion-collapse collapse ${ActiveTab === "image" ? "show" : ""}`}
                         aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div className="accordion-body">

                            <ImageSubmitComponent/>

                        </div>
                    </div>
                </div>


                <div className="accordion-item">
                    <h2 className="accordion-header" id="aboutHeading">
                        <button className={`accordion-button ${ActiveTab === "textString" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextString"
                                aria-expanded={ActiveTab === "textString" ? "true" : "false"}
                                aria-controls="collapseAbout"
                                onClick={() => setActiveTab("textString")}
                        >
                            Post Raw Text To Comprehend
                        </button>
                    </h2>
                    <div id="collapseLinks"
                         className={`accordion-collapse collapse  ${ActiveTab === "textString" ? "show" : ""}`}
                         aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                        <div className="accordion-body">

                            <StringSubmitComponant/>
                        </div>
                    </div>
                </div>


                <div className="accordion-item mb-5">
                    <h2 className="accordion-header" id="linksHeading">
                        <button className={`accordion-button ${ActiveTab === "textFile" ? "" : "collapsed"}`}
                                type="button" data-bs-toggle="collapse" data-bs-target="#collapsetextFile"
                                aria-expanded={ActiveTab === "textFile" ? "true" : "false"}
                                aria-controls="collapseLinks"
                                onClick={() => setActiveTab("textFile")}
                        >
                            Post Text File To Comprehend
                        </button>
                    </h2>
                    <div id="collapseLinks"
                         className={`accordion-collapse collapse ${ActiveTab === "textFile" ? "show" : ""}`}
                         aria-labelledby="headingLinks" data-bs-parent="#accordionExample">
                        <div className="accordion-body">

                            <TextfileSubmitComponant />

                        </div>
                    </div>
                </div>
            </div>

        </div>


    );


}

export default SubmitPage;
