/**
 * The SubmitStringPage script builds a small page that shows the settings json file. This page and its functionality is
 * realtively obsolete as editing the JSON file via React proved hard therefore was not implemented as a feature
  *
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: removed a number of end points as well as the hard coded
 *                                      URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */

import React from 'react';

// get the setting JSON data
import SettingsJSON from "../Settings/SiteSettings.json";

function SubmitStringPage() {
   return (
        <div className="container">
            <div className="card border-secondary mb-3">
                <h4 className="card-header">Settings</h4>
                <div className="card-body">
                    <fieldset>
                        {
                            SettingsJSON &&
                            Object.keys(SettingsJSON).map( function (key,index) {

                                    return <div key={"FormElem_"+SettingsJSON[key].title} className="form-group row my-2">
                                        <label htmlFor={SettingsJSON[key].title} className="col-sm-2 col-form-label">
                                            {SettingsJSON[key].label}
                                        </label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control"
                                                   label={SettingsJSON[key].label}
                                                   id={SettingsJSON[key].title}
                                                   defaultValue={SettingsJSON[key].value}
                                                    />
                                        </div>
                                    </div>
                                }
                            )
                        }
                    </fieldset>
                </div>
            </div>
        </div>
    );
}

export default SubmitStringPage;
