 /**
 * The ReportListItem script builds each individual report entry on the reports page
 * The data comes into this section via a prop
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */



import React from "react";

function ReportListItem(props) {

    // store the Job ID in a const
    const reportID = props.ReportID;

    // store the rest if the data in a const
    const reportData = props.ReportData;

    // initialise a data variable
    let startDate = '';

    // if there is data to show, prep it for display
    if( reportData !== undefined ){
        if(reportData['WebUIInput'] !== undefined) {
            // get the data of the process time
            startDate = reportData['WebUIImage']['StartDateTime'];
        } else if(reportData['Comprehend'] !== undefined){
            // get the data of the process time
            startDate = reportData['Comprehend']['StartDateTime'];
        }
    }

    // construct a function so that we can respond and move into the Job reporting pages
    const viewJobReport = (reportID) => {

        // build URL
        const url = '/reporting/' + reportID;

        // redirect page
        window.location.href = url;
    }

    return (
        <tr
            onClick={() => viewJobReport(reportID)}
            className="cursor-pointer"
        >
            <th scope="row">{reportID}</th>
            <td>{startDate}</td>
        </tr>
    )
}

export default ReportListItem;

