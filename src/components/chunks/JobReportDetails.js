/**
 * The JobReportDetails script builds the surrounding container table for a individual job report.
 * The basic job data is passed into this page as a prop. Clicking on a view button will load data from the API
 *
 * @author  Ian Blott
 * @version 1.0
 * @since   2021-9-12
 *
 * @edits :: 2021-10-11 :: Ian Blott :: add dynamic URL for AWS API location
 *           2021-10-24 :: Ian Blott :: added comments and cleaned up code for alpha release
 *
 */



import React, {useState} from "react";
import {AiFillEye} from "react-icons/ai";
import SettingsJSON from "../Settings/SiteSettings.json";

function JobReportDetails(props) {

    // needed so we know what job to get from the DB
    const jobId = props.jobId;

    // needed so we know what field to get from the DB
    const Lambda = props.Lambda;

    // the name of the input file
    const InputFile = props.InputFile;

    // the name of the output file
    const OutputFile = props.OutputFile;

    // the job date
    const StartDateTime = props.StartDateTime;

    // state to store the input file contents
    const [contentInputFile, SetcontentInputFile] = useState("");

    // state to store the output file contents
    const [contentOutputFile, setContentOutputFile] = useState("");

    // get the deployment details for the API URL
    const REACT_APP_API_GATEWAY_ID  = process.env.REACT_APP_API_GATEWAY_ID

    // create a click response function to be called when the use clicks on the "view file" button
    const readFile = (fieldName) => {

        // construct the API endpoint URL
        const url = "https://"+REACT_APP_API_GATEWAY_ID+".execute-api.ap-southeast-2.amazonaws.com/Stage" +
                        SettingsJSON['ViewReport']['value'] +
                        '?LambdaName=' + Lambda +
                        '&JobId=' + jobId +
                        '&FieldName=' + fieldName;

        // get the data from the API
        fetch(url, {mode: 'cors'})
            .then(function (response) {
                // decode the response into a JSON object
                return response.json();
            })
            .then(function (myJson) {

                // decide on what data the data is for
                switch (fieldName) {

                    // set the respective state contents based on the field name give in the function params
                    case "InputFile":
                        SetcontentInputFile(myJson.body);
                        break;
                    case "OutputFile":
                        setContentOutputFile(myJson.body);
                        break;

                    default:
                        break;
                }

            });
    }
    return (
        <div>

            <table className="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Field</th>
                    <th scope="col">Data</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <th scope="row" rowSpan="3">{Lambda}</th>
                    <td>InputFile</td>
                    <td>
                        {InputFile}

                        {
                        // only include the section if there is data to show
                        contentInputFile &&
                        <div className="card border-secondary mb-3 file-content-view">
                            <div className="card-header">File Content</div>
                            <div className="card-body">
                                <pre>{contentInputFile}</pre>
                            </div>
                        </div>
                        }
                    </td>
                    <td>
                        {
                        // only include the view button on Lambda's sections that store to or read from a file
                            Lambda !== "WebUIImage" &&   Lambda !== "WebUIString" && InputFile !== "" && contentInputFile === "" &&
                        <button type="button"
                                className="btn btn-primary"
                                data-toggle="modal"
                                data-target="#ViewReport"
                                onClick={(e) => readFile('InputFile')}
                        >
                            <AiFillEye className="AiFillEye"/>
                            View
                        </button>
                        }

                    </td>
                </tr>
                <tr>
                    <td>OutputFile</td>
                    <td>
                        {OutputFile}
                        {
                        // only include the section if there is data to show
                        contentOutputFile &&
                        <div className="card border-secondary mb-3 file-content-view">
                            <div className="card-header">File Content</div>
                            <div className="card-body">
                                <pre>{contentOutputFile}</pre>
                            </div>
                        </div>
                        }
                    </td>
                    <td>
                        {
                        // only include the view button on Lambda's sections that store to or read from a file
                        Lambda !== "WebUIImage" &&  Lambda !== "APIInput" && Lambda !== "Comprehend" && OutputFile !== "" && contentOutputFile === "" &&
                        <button type="button"
                                className="btn btn-primary"
                                data-toggle="modal"
                                data-target="#ViewReport"
                                onClick={(e) => readFile('OutputFile')}
                        >
                            <AiFillEye className="AiFillEye"/>
                            View
                        </button>
                        }

                    </td>
                </tr>
                <tr>
                    <td>StartDateTime</td>
                    <td>{StartDateTime}</td>
                    <td></td>
                </tr>

                </tbody>
            </table>

        </div>
    )
}


export default JobReportDetails;